# khan-academy-odoo

Created an academy module where there are 4 menus for Students, Teachers, Courses, Exams.
Each menu have different views (Tree, Form, search).

## 1. Students model & view: Here all the students information will show and there is an option for creating new student information.

![Student1](./picture/1.png)
![Student2](./picture/2.png)

## 2. Courses model & view: Here all the courses information will show and there is an option for creating new course information.

![Course1](./picture/3.png)
![Course2](./picture/4.png)

## 3. Teachers model & view: Here all the teachers information will show and there is an option for creating new teacher information.

![Teachers1](./picture/5.png)
![Teachers2](./picture/6.png)

## 4. Exams model & view: Here all the exams information will show and there is an option for creating new exam information.

![Exams1](./picture/7.png)
![Exams2](./picture/8.png)
