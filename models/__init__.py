# -*- coding: utf-8 -*-

from . import student
from . import course
from . import teacher
from . import exam
